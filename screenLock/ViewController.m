//
//  ViewController.m
//  screenLock
//
//  Created by click labs 115 on 11/28/15.
//  Copyright © 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"
int selectNumber;
NSString *convert;
NSString *password;
@interface ViewController ()
@property (strong, nonatomic) IBOutlet UITextField *textBox;
@property (strong, nonatomic) IBOutlet UIButton *one;
@property (strong, nonatomic) IBOutlet UIButton *two;
@property (strong, nonatomic) IBOutlet UIButton *three;
@property (strong, nonatomic) IBOutlet UIButton *four;
@property (strong, nonatomic) IBOutlet UIButton *five;
@property (strong, nonatomic) IBOutlet UIButton *six;
@property (strong, nonatomic) IBOutlet UIButton *seven;
@property (strong, nonatomic) IBOutlet UIButton *eight;
@property (strong, nonatomic) IBOutlet UIButton *nine;
@property (strong, nonatomic) IBOutlet UIButton *zero;
@property (strong, nonatomic) IBOutlet UIButton *ok;
@property (strong, nonatomic) IBOutlet UIButton *clearBtn;

@end

@implementation ViewController
@synthesize one;
@synthesize two;
@synthesize three;
@synthesize four;
@synthesize five;
@synthesize six;
@synthesize seven;
@synthesize eight;
@synthesize nine;
@synthesize zero;
@synthesize ok;
@synthesize textBox;
@synthesize clearBtn;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    textBox.secureTextEntry=YES;
}
- (IBAction)oneBtn:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber+1;
    convert=[NSString stringWithFormat:@"%d",selectNumber];
    textBox.text=[NSString stringWithFormat:@"%@",convert];
    
    
}
- (IBAction)twoBtn:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber+2;
    convert=[NSString stringWithFormat:@"%d",selectNumber];
    textBox.text=[NSString stringWithFormat:@"%@",convert];
}
- (IBAction)threeBtn:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber+3;
    convert=[NSString stringWithFormat:@"%d",selectNumber];
    textBox.text=[NSString stringWithFormat:@"%@",convert];
}
- (IBAction)fourBtn:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber+4;
    convert=[NSString stringWithFormat:@"%d",selectNumber];
    textBox.text=[NSString stringWithFormat:@"%@",convert];
}
- (IBAction)fiveBtn:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber+5;
    convert=[NSString stringWithFormat:@"%d",selectNumber];
    textBox.text=[NSString stringWithFormat:@"%@",convert];
}
- (IBAction)sixBtn:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber+6;
    convert=[NSString stringWithFormat:@"%d",selectNumber];
    textBox.text=[NSString stringWithFormat:@"%@",convert];
}
- (IBAction)sevenBtn:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber+7;
    convert=[NSString stringWithFormat:@"%d",selectNumber];
    textBox.text=[NSString stringWithFormat:@"%@",convert];
}
- (IBAction)eightBtn:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber+8;
    convert=[NSString stringWithFormat:@"%d",selectNumber];
    textBox.text=[NSString stringWithFormat:@"%@",convert];
}
- (IBAction)nineBtn:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber+9;
    convert=[NSString stringWithFormat:@"%d",selectNumber];
    textBox.text=[NSString stringWithFormat:@"%@",convert];
}
- (IBAction)zeroBtn:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber+0;
    convert=[NSString stringWithFormat:@"%d",selectNumber];
    textBox.text=[NSString stringWithFormat:@"%@",convert];
}
- (IBAction)okBtn:(id)sender {
    password=[NSString stringWithFormat:@"1212"];
    if ([textBox.text containsString:password]) {
        NSLog(@"you are logged in");
    } else {
        NSLog(@"you are not logged in");
    }
    
 }
- (IBAction)clearTextField:(id)sender {
    textBox.text=@"";

  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
